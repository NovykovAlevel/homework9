package com.collector_agency.banks;

import java.util.HashMap;

public class Bank<T> {
    private T bankName;
    private Integer creditLimit;

    private HashMap<String, Integer> users = new HashMap<>();

    public Bank(T bankName, Integer creditLimit) {
        this.bankName = bankName;
        this.creditLimit = creditLimit;
    }

    public T getBankName() {
        return bankName;
    }

    public Integer getCreditLimit() {
        return creditLimit;
    }

    public void addNewUser(String name, Integer creditSum) {
        users.put(name, creditSum);
    }

    public boolean isUserClient(String userName) {
        return this.users.containsKey(userName);
    }
}
